import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class analyzes a large text file and sorts the words and/or characters
 * of the file
 * 
 * @author Tyler Tracey, Keith Roberts
 * @version 1.2.4
 *
 */
public class Analyzer {

    private static File textFile;
    private static Scanner scanFile;
    static ArrayList<Word> wordBank = new ArrayList<Word>();

    /**
     * This method sets the file for the program to parse through
     * 
     * @param set
     *            is a text file for the program to read
     */
    public static void setFile(File set) {
        textFile = set;
        try {
            scanFile = new Scanner(textFile);
        }
        catch (FileNotFoundException e) {
            System.out.print("File not found.");
            e.printStackTrace();
        }
    }

    /**
     * This method populates the array list with the words from the text file
     */
    public static void findWords() {
        while (scanFile.hasNextLine()) {
            String line = scanFile.nextLine();
            @SuppressWarnings("resource")
            Scanner scan = new Scanner(line);
            while (scan.hasNext()) {
                String notForm = scan.next();
                String next = formatted(notForm);
                boolean isNew = checkWord(next);
                if (isNew == true) {
                    wordBank.add(new Word(next));
                }
                else {
                    // do nothing
                }
            }
        }
    }

    /**
     * This method edits the string to remove any punctuation from the end of
     * the String
     * 
     * @param next
     *            is the word to be edited
     * @return String of the word given, without unnecessary punctuation
     */
    private static String formatted(String next) {
        String form = next.toLowerCase()
                .replaceAll("[,'.(-12345#;678%9&~_:`0)?!\"]", "");
        return form;
    }

    /**
     * This method checks to see if the word being scanned is already a member
     * of the array list
     * 
     * @param next
     *            is the string to be checked
     * @return boolean false if the word is already a member of the list, true
     *         otherwise. also increments the word objects count field if it
     *         already exists within the array list
     */
    private static boolean checkWord(String next) {
        for (int i = 0; i < wordBank.size(); i++) {
            if (wordBank.get(i).getName().equals(next)) {
                wordBank.get(i).addOccurence();
                return false;
            }
        }
        return true;
    }

    /**
     * This method returns the most frequently occurring words in the text file
     * 
     * @param show
     *            is the number of top words to display
     * @return returnWords is an array of the top words and their number of
     *         occurrences
     */
    public static Word[] findTopWords(int show) {
        Word[] returnWords = new Word[show];
        if (show <= 0) {
            throw new IllegalArgumentException();
        }
        int low = 0;
        int high = wordBank.size() - 1;
        quickSortByWords(wordBank, low, high);
        for (int i = 0; i < returnWords.length; i++) {
            returnWords[i] = wordBank.get(wordBank.size() - (i + 1));
        }

        return returnWords;
    }

    /**
     * This quick sort method sorts the array list based on the number of word
     * occurrences
     * 
     * @param list
     *            for the algorithm to sort
     * @param start
     *            is the beginning index of the list to sort
     * @param end
     *            is the last index of the list to sort
     */
    public static void quickSortByWords(ArrayList<Word> list, int start,
            int end) {
        int low = start;
        int high = end;
        if (end - start >= 1) {
            Word pivot = list.get(low);
            while (high > low) {
                while (list.get(low).getCount() <= pivot.getCount()
                        && low <= end && high > low)
                    low++;
                while (list.get(high).getCount() > pivot.getCount()
                        && high >= start && high >= low)
                    high--;
                if (high > low)
                    swap(list, low, high);
            }
            swap(list, start, high);
            quickSortByWords(list, start, high - 1);
            quickSortByWords(list, high + 1, end);
        }
        else {
            return;
        }
    }

    /**
     * This method is used by the quick sort methods to swap the elements in the
     * list depending on which value is greater
     * 
     * @param list
     *            the list that is being modified
     * @param index1
     *            the index on the lower half
     * @param index2
     *            the index on the upper half
     */
    public static void swap(ArrayList<Word> list, int index1, int index2) {
        Word tmp = list.get(index1);
        list.set(index1, list.get(index2));
        list.set(index2, tmp);
    }

}