import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class AnalyzerGUI extends JFrame {

    public AnalyzerGUI() {

        super("Text Analyzer");
        setLocationRelativeTo(null);
        setSize(600, 400);
        setLayout(new BorderLayout());
        setResizable(false);

        JPanel northPanel = new JPanel();
        northPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        northPanel.setBackground(Color.gray);
        northPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        add(northPanel, BorderLayout.NORTH);

        JButton chooseFileButton = new JButton("Choose File...");
        northPanel.add(chooseFileButton);
        chooseFileButton.setVisible(true);

        JLabel chooseFileLabel = new JLabel();
        chooseFileLabel.setText("Please choose a text file to analyze.");
        northPanel.add(chooseFileLabel);
        chooseFileLabel.setVisible(true);

        // East Box Creation
        Box westBox = Box.createVerticalBox();
        add(westBox, BorderLayout.WEST);

        JButton allWords = new JButton("Create WordList");
        westBox.add(allWords);
        allWords.setEnabled(false);
        allWords.setVisible(true);

        JButton top10Words = new JButton("Top 10 Words");
        westBox.add(top10Words);
        top10Words.setEnabled(false);
        top10Words.setVisible(true);

        // Table Model
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Word / Character");
        model.addColumn("Occurrences");

        // Center Table
        JTable mainTable = new JTable(model);
        mainTable.setPreferredScrollableViewportSize(new Dimension(500, 100));
        mainTable.setFillsViewportHeight(true);
        mainTable.setGridColor(Color.black);
        add(mainTable.getTableHeader(), BorderLayout.CENTER);
        add(new JScrollPane(mainTable));

        chooseFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        JFileChooser openChoose = new JFileChooser();
                        openChoose.setCurrentDirectory(new java.io.File(
                                "/Users/traceys5/Desktop/SongLibrary"));
                        openChoose.setDialogTitle("Open");
                        int value = openChoose.showOpenDialog(null);
                        if (value == JFileChooser.APPROVE_OPTION) {

                            File tmp = openChoose.getSelectedFile();
                            Analyzer.setFile(tmp);
                            Analyzer.findWords();
                            northPanel.setBackground(Color.green);
                            chooseFileLabel.setText(tmp.getAbsolutePath());
                            allWords.setEnabled(true);
                            top10Words.setEnabled(true);

                        }

                    }

                });
            }

        });

        allWords.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setRowCount(0);
                int low = 0;
                int high = Analyzer.wordBank.size() - 1;
                Analyzer.quickSortByWords(Analyzer.wordBank, low, high);
                for (int i = high; i > low; i--) {
                    model.addRow(
                            new Object[] { Analyzer.wordBank.get(i).getName(),
                                    Analyzer.wordBank.get(i).getCount() });
                }

            }
        });

        top10Words.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setRowCount(0);
                Word[] top10 = Analyzer.findTopWords(10);
                for (int i = 0; i < top10.length; i++) {
                    model.addRow(new Object[] { top10[i].getName(),
                            top10[i].getCount() });
                }
            }
        });
    }

    public static void main(String args[]) {
        AnalyzerGUI show = new AnalyzerGUI();
        show.setVisible(true);
    }

}