/**
 * Object class for specific word instances
 * 
 * @author tyler_tracey
 * @version 1.1.1
 *
 */
public class Word {

    /**
     * Name of the word
     */
    private String wordName;
    /**
     * Number of word occurrences
     */
    private int count;

    /**
     * 
     * @param name
     *            String of the word
     */
    public Word(String name) {
        wordName = name;
        count = 1;
    }

    /**
     * Getter method for word name
     * 
     * @return name String of the word
     */
    public String getName() {
        return wordName;
    }

    /**
     * Getter method for the number of word occurrences
     * 
     * @return count integer number of occurrences
     */
    public int getCount() {
        return count;
    }

    /**
     * Void method to increment the word count
     */
    public void addOccurence() {
        count = count + 1;
    }
}